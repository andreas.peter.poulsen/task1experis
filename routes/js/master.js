$.ajax({
    url: "/hi",
    dataType:'json',
    cache: false,
    success: function(data){
        data = JSON.parse(data);
        let Table = $('#tr1');

        $(data).each(function (index, element) {
            $('#tr1').append($('<tr>')
            .append($('<td>').append(element.name))
            .append($('<td>').append(element.age))
            .append($('<td>').append(element.type)))
        });
    },

    error: function (err) {
        console.log(err);
    }
});