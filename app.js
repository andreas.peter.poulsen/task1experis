/*********************************/
/* MODULES/DEPENDANCIES */
/*********************************/
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var querystring = require('querystring');
var http = require('http');
var fs = require('fs');
var bodyParser = require('body-parser');


var app = express();
app.listen(process.env.PORT || 8080);

/*********************************/
/* PROPORTIES */
/*********************************/



/*********************************/
/* INIT */
/*********************************/
(function init() {
  engineSetup();
  restAPIgetJson();
  routesSetup();
  catch404();
  errorHandling();
  
})();

/*********************************/
/* OWN FUNCTIONS */
/*********************************/
function jsonFile(fileName){
  var contents = fs.readFileSync(__dirname + '/public/json/' + fileName + ".json", 'utf8');
  return contents;
}

function restAPIgetJson(){ 
  let jsonData = {};
  jsonData = jsonFile("sample");
  console.log(jsonData);

  app.get("/hi", (req, res) => {
    res.json(jsonData);
  });
}

function engineSetup(){
  // view engine setup
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'jade');

  app.use(logger('dev'));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(cookieParser());
}

function routesSetup(){
  // Set routes
  app.use(express.static(path.join(__dirname + '/public')));  // for html files and such
  app.use(express.static(path.join(__dirname + '/static')));  // for json files and such
  app.use(express.static(path.join(__dirname + '/routes')));  // for json files and such

  // Set pages
  app.get('/', (req, res) => { res.sendFile(path.join(__dirname + '/routes/html/index.html'))});
  app.get('/info.html', (req, res) => {  res.sendFile(path.join(__dirname + '/routes/html/info.html'));});
  app.get('/pictures.html', (req, res) => { res.sendFile(path.join(__dirname + '/routes/html/pictures.html'))});

  // Json page
  app.get('/data', (req, res) => { res.sendFile(path.join(__dirname + '/public/json/sample.json'))});
}

function catch404(){
  // catch 404 and forward to error handler
  app.use(function(req, res, next) {
    next(createError(404));
  });
}

function errorHandling(){
  // error handler
  app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });
}

module.exports = app;
